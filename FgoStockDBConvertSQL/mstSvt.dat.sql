INSERT INTO items (itemid, importance, description)
VALUES (100100, 16, 'アルトリア・ペンドラゴン');
INSERT INTO items (itemid, importance, description)
VALUES (100200, 12, 'アルトリア・ペンドラゴン〔オルタ〕');
INSERT INTO items (itemid, importance, description)
VALUES (100300, 12, 'アルトリア・ペンドラゴン〔リリィ〕');
INSERT INTO items (itemid, importance, description)
VALUES (100500, 12, 'ネロ・クラウディウス');
INSERT INTO items (itemid, importance, description)
VALUES (100800, 12, 'ジークフリート');
INSERT INTO items (itemid, importance, description)
VALUES (101300, 7, 'ガイウス・ユリウス・カエサル');
INSERT INTO items (itemid, importance, description)
VALUES (101800, 16, 'アルテラ');
INSERT INTO items (itemid, importance, description)
VALUES (102200, 7, 'ジル・ド・レェ');
INSERT INTO items (itemid, importance, description)
VALUES (102600, 12, 'シュヴァリエ・デオン');
INSERT INTO items (itemid, importance, description)
VALUES (200100, 12, 'エミヤ');
INSERT INTO items (itemid, importance, description)
VALUES (200200, 16, 'ギルガメッシュ');
INSERT INTO items (itemid, importance, description)
VALUES (200300, 7, 'ロビンフッド');
INSERT INTO items (itemid, importance, description)
VALUES (200500, 12, 'アタランテ');
INSERT INTO items (itemid, importance, description)
VALUES (201200, 7, 'エウリュアレ');
INSERT INTO items (itemid, importance, description)
VALUES (201300, 3, 'アーラシュ');
INSERT INTO items (itemid, importance, description)
VALUES (300100, 7, 'クー・フーリン');
INSERT INTO items (itemid, importance, description)
VALUES (300500, 12, 'エリザベート・バートリー');
INSERT INTO items (itemid, importance, description)
VALUES (300600, 4, '武蔵坊弁慶');
INSERT INTO items (itemid, importance, description)
VALUES (300700, 7, 'クー・フーリン〔プロトタイプ〕');
INSERT INTO items (itemid, importance, description)
VALUES (300900, 4, 'レオニダス一世');
INSERT INTO items (itemid, importance, description)
VALUES (301000, 7, 'ロムルス');
INSERT INTO items (itemid, importance, description)
VALUES (400100, 7, 'メドゥーサ');
INSERT INTO items (itemid, importance, description)
VALUES (400600, 4, 'ゲオルギウス');
INSERT INTO items (itemid, importance, description)
VALUES (400800, 4, 'エドワード・ティーチ');
INSERT INTO items (itemid, importance, description)
VALUES (401100, 7, 'ブーディカ');
INSERT INTO items (itemid, importance, description)
VALUES (401400, 7, '牛若丸');
INSERT INTO items (itemid, importance, description)
VALUES (401500, 7, 'アレキサンダー');
INSERT INTO items (itemid, importance, description)
VALUES (401700, 12, 'マリー・アントワネット');
INSERT INTO items (itemid, importance, description)
VALUES (401900, 12, 'マルタ');
INSERT INTO items (itemid, importance, description)
VALUES (500100, 7, 'メディア');
INSERT INTO items (itemid, importance, description)
VALUES (500200, 7, 'ジル・ド・レェ');
INSERT INTO items (itemid, importance, description)
VALUES (500500, 4, 'ハンス・クリスチャン・アンデルセン');
INSERT INTO items (itemid, importance, description)
VALUES (500700, 4, 'ウィリアム・シェイクスピア');
INSERT INTO items (itemid, importance, description)
VALUES (501400, 7, 'メフィストフェレス');
INSERT INTO items (itemid, importance, description)
VALUES (501500, 3, 'ヴォルフガング・アマデウス・モーツァルト');
INSERT INTO items (itemid, importance, description)
VALUES (501900, 16, '諸葛孔明〔エルメロイⅡ世〕');
INSERT INTO items (itemid, importance, description)
VALUES (502100, 7, 'クー・フーリン');
INSERT INTO items (itemid, importance, description)
VALUES (600100, 3, '佐々木小次郎');
INSERT INTO items (itemid, importance, description)
VALUES (600200, 4, '呪腕のハサン');
INSERT INTO items (itemid, importance, description)
VALUES (601000, 12, 'ステンノ');
INSERT INTO items (itemid, importance, description)
VALUES (601100, 7, '荊軻');
INSERT INTO items (itemid, importance, description)
VALUES (601200, 4, 'シャルル＝アンリ・サンソン');
INSERT INTO items (itemid, importance, description)
VALUES (601300, 4, 'ファントム・オブ・ジ・オペラ');
INSERT INTO items (itemid, importance, description)
VALUES (601400, 3, 'マタ・ハリ');
INSERT INTO items (itemid, importance, description)
VALUES (601700, 12, 'カーミラ');
INSERT INTO items (itemid, importance, description)
VALUES (700100, 12, 'ヘラクレス');
INSERT INTO items (itemid, importance, description)
VALUES (700200, 12, 'ランスロット');
INSERT INTO items (itemid, importance, description)
VALUES (700300, 7, '呂布奉先');
INSERT INTO items (itemid, importance, description)
VALUES (700500, 3, 'スパルタクス');
INSERT INTO items (itemid, importance, description)
VALUES (700600, 16, '坂田金時');
INSERT INTO items (itemid, importance, description)
VALUES (700700, 16, 'ヴラド三世');
INSERT INTO items (itemid, importance, description)
VALUES (700900, 3, 'アステリオス');
INSERT INTO items (itemid, importance, description)
VALUES (701000, 4, 'カリギュラ');
INSERT INTO items (itemid, importance, description)
VALUES (701100, 7, 'ダレイオス三世');
INSERT INTO items (itemid, importance, description)
VALUES (701300, 7, '清姫');
INSERT INTO items (itemid, importance, description)
VALUES (701500, 4, 'エイリーク・ブラッドアクス');
INSERT INTO items (itemid, importance, description)
VALUES (701600, 12, 'タマモキャット');
INSERT INTO items (itemid, importance, description)
VALUES (800100, 0, 'マシュ・キリエライト');
INSERT INTO items (itemid, importance, description)
VALUES (900100, 16, 'ジャンヌ・ダルク');
INSERT INTO items (itemid, importance, description)
VALUES (900300, 12, 'ジャンヌ・ダルク〔オルタ〕');
INSERT INTO items (itemid, importance, description)
VALUES (9400010, 1, '頑強');
INSERT INTO items (itemid, importance, description)
VALUES (9400020, 1, '瞑想');
INSERT INTO items (itemid, importance, description)
VALUES (9400030, 1, '技巧');
INSERT INTO items (itemid, importance, description)
VALUES (9400040, 1, '先制');
INSERT INTO items (itemid, importance, description)
VALUES (9400050, 1, '破壊');
INSERT INTO items (itemid, importance, description)
VALUES (9400060, 3, '閃光');
INSERT INTO items (itemid, importance, description)
VALUES (9400070, 3, '好機');
INSERT INTO items (itemid, importance, description)
VALUES (9400080, 3, '豊穣');
INSERT INTO items (itemid, importance, description)
VALUES (9400090, 3, '集中');
INSERT INTO items (itemid, importance, description)
VALUES (9400100, 3, '天啓');
INSERT INTO items (itemid, importance, description)
VALUES (9400110, 5, 'アゾット剣');
INSERT INTO items (itemid, importance, description)
VALUES (9400120, 5, '偽臣の書');
INSERT INTO items (itemid, importance, description)
VALUES (9400130, 5, '青の黒鍵');
INSERT INTO items (itemid, importance, description)
VALUES (9400140, 5, '緑の黒鍵');
INSERT INTO items (itemid, importance, description)
VALUES (9400150, 5, '赤の黒鍵');
INSERT INTO items (itemid, importance, description)
VALUES (9400160, 5, '凛のペンダント');
INSERT INTO items (itemid, importance, description)
VALUES (9400170, 5, '魔導書');
INSERT INTO items (itemid, importance, description)
VALUES (9400180, 5, '龍脈');
INSERT INTO items (itemid, importance, description)
VALUES (9400190, 5, '魔術鉱石');
INSERT INTO items (itemid, importance, description)
VALUES (9400200, 5, '竜種');
INSERT INTO items (itemid, importance, description)
VALUES (9400210, 9, '鋼の鍛錬');
INSERT INTO items (itemid, importance, description)
VALUES (9400220, 9, '原始呪術');
INSERT INTO items (itemid, importance, description)
VALUES (9400230, 9, '投影魔術');
INSERT INTO items (itemid, importance, description)
VALUES (9400240, 9, 'ガンド');
INSERT INTO items (itemid, importance, description)
VALUES (9400250, 9, '緑の破音');
INSERT INTO items (itemid, importance, description)
VALUES (9400260, 9, '宝石魔術・対影');
INSERT INTO items (itemid, importance, description)
VALUES (9400270, 9, '優雅たれ');
INSERT INTO items (itemid, importance, description)
VALUES (9400280, 9, '虚数魔術');
INSERT INTO items (itemid, importance, description)
VALUES (9400290, 9, '天の晩餐');
INSERT INTO items (itemid, importance, description)
VALUES (9400300, 9, '天使の詩');
INSERT INTO items (itemid, importance, description)
VALUES (9400310, 12, 'フォーマルクラフト');
INSERT INTO items (itemid, importance, description)
VALUES (9400320, 12, 'イマジナリ・アラウンド');
INSERT INTO items (itemid, importance, description)
VALUES (9400330, 12, 'リミテッド／ゼロオーバー');
INSERT INTO items (itemid, importance, description)
VALUES (9400340, 12, 'カレイドスコープ');
INSERT INTO items (itemid, importance, description)
VALUES (9400350, 12, 'ヘヴンスフィール');
INSERT INTO items (itemid, importance, description)
VALUES (9400360, 3, '旅の始まり');
INSERT INTO items (itemid, importance, description)
VALUES (9501100, 0, '英霊結晶・宵闇のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9501200, 0, '英霊結晶・煌めくフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9501300, 0, '英霊結晶・星のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9501400, 0, '英霊結晶・天駆けるフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9501500, 0, '-');
INSERT INTO items (itemid, importance, description)
VALUES (9502100, 0, '英霊結晶・宵闇のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9502200, 0, '英霊結晶・煌めくフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9502300, 0, '英霊結晶・星のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9502400, 0, '英霊結晶・天駆けるフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9502500, 0, '-');
INSERT INTO items (itemid, importance, description)
VALUES (9503100, 0, '英霊結晶・宵闇のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9503200, 0, '英霊結晶・煌めくフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9503300, 0, '英霊結晶・星のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9503400, 0, '英霊結晶・天駆けるフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9503500, 0, '-');
INSERT INTO items (itemid, importance, description)
VALUES (9504100, 0, '英霊結晶・宵闇のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9504200, 0, '英霊結晶・煌めくフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9504300, 0, '英霊結晶・星のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9504400, 0, '英霊結晶・天駆けるフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9504500, 0, '-');
INSERT INTO items (itemid, importance, description)
VALUES (9505100, 0, '英霊結晶・宵闇のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9505200, 0, '英霊結晶・煌めくフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9505300, 0, '英霊結晶・星のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9505400, 0, '英霊結晶・天駆けるフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9505500, 0, '-');
INSERT INTO items (itemid, importance, description)
VALUES (9506100, 0, '英霊結晶・宵闇のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9506200, 0, '英霊結晶・煌めくフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9506300, 0, '英霊結晶・星のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9506400, 0, '英霊結晶・天駆けるフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9506500, 0, '-');
INSERT INTO items (itemid, importance, description)
VALUES (9507100, 0, '英霊結晶・宵闇のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9507200, 0, '英霊結晶・煌めくフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9507300, 0, '英霊結晶・星のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9507400, 0, '英霊結晶・天駆けるフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9507500, 0, '-');
INSERT INTO items (itemid, importance, description)
VALUES (9550100, 0, '英霊結晶・宵闇のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9550200, 0, '英霊結晶・煌めくフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9550300, 0, '英霊結晶・星のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9550400, 0, '英霊結晶・天駆けるフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9550500, 0, '-');
INSERT INTO items (itemid, importance, description)
VALUES (9570100, 0, '英霊結晶・宵闇のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9570200, 0, '英霊結晶・煌めくフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9570300, 0, '英霊結晶・星のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9570400, 0, '英霊結晶・天駆けるフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9570500, 0, '-');
INSERT INTO items (itemid, importance, description)
VALUES (9601100, 0, '英霊結晶・明けのフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9601200, 0, '英霊結晶・輝けるフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9601300, 0, '英霊結晶・太陽のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9601400, 0, '英霊結晶・天照らすフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9601500, 0, '-');
INSERT INTO items (itemid, importance, description)
VALUES (9602100, 0, '英霊結晶・明けのフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9602200, 0, '英霊結晶・輝けるフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9602300, 0, '英霊結晶・太陽のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9602400, 0, '英霊結晶・天照らすフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9602500, 0, '-');
INSERT INTO items (itemid, importance, description)
VALUES (9603100, 0, '英霊結晶・明けのフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9603200, 0, '英霊結晶・輝けるフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9603300, 0, '英霊結晶・太陽のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9603400, 0, '英霊結晶・天照らすフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9603500, 0, '-');
INSERT INTO items (itemid, importance, description)
VALUES (9604100, 0, '英霊結晶・明けのフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9604200, 0, '英霊結晶・輝けるフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9604300, 0, '英霊結晶・太陽のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9604400, 0, '英霊結晶・天照らすフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9604500, 0, '-');
INSERT INTO items (itemid, importance, description)
VALUES (9605100, 0, '英霊結晶・明けのフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9605200, 0, '英霊結晶・輝けるフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9605300, 0, '英霊結晶・太陽のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9605400, 0, '英霊結晶・天照らすフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9605500, 0, '-');
INSERT INTO items (itemid, importance, description)
VALUES (9606100, 0, '英霊結晶・明けのフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9606200, 0, '英霊結晶・輝けるフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9606300, 0, '英霊結晶・太陽のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9606400, 0, '英霊結晶・天照らすフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9606500, 0, '-');
INSERT INTO items (itemid, importance, description)
VALUES (9607100, 0, '英霊結晶・明けのフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9607200, 0, '英霊結晶・輝けるフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9607300, 0, '英霊結晶・太陽のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9607400, 0, '英霊結晶・天照らすフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9607500, 0, '-');
INSERT INTO items (itemid, importance, description)
VALUES (9650100, 0, '英霊結晶・明けのフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9650200, 0, '英霊結晶・輝けるフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9650300, 0, '英霊結晶・太陽のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9650400, 0, '英霊結晶・天照らすフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9650500, 0, '-');
INSERT INTO items (itemid, importance, description)
VALUES (9670100, 0, '英霊結晶・明けのフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9670200, 0, '英霊結晶・輝けるフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9670300, 0, '英霊結晶・太陽のフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9670400, 0, '英霊結晶・天照らすフォウくん');
INSERT INTO items (itemid, importance, description)
VALUES (9670500, 0, '-');
INSERT INTO items (itemid, importance, description)
VALUES (9701100, 0, '叡智の種火');
INSERT INTO items (itemid, importance, description)
VALUES (9701200, 0, '叡智の灯火');
INSERT INTO items (itemid, importance, description)
VALUES (9701300, 0, '叡智の大火');
INSERT INTO items (itemid, importance, description)
VALUES (9701400, 0, '叡智の猛火');
INSERT INTO items (itemid, importance, description)
VALUES (9701500, 0, '叡智の聖火');
INSERT INTO items (itemid, importance, description)
VALUES (9702100, 0, '叡智の種火');
INSERT INTO items (itemid, importance, description)
VALUES (9702200, 0, '叡智の灯火');
INSERT INTO items (itemid, importance, description)
VALUES (9702300, 0, '叡智の大火');
INSERT INTO items (itemid, importance, description)
VALUES (9702400, 0, '叡智の猛火');
INSERT INTO items (itemid, importance, description)
VALUES (9702500, 0, '叡智の聖火');
INSERT INTO items (itemid, importance, description)
VALUES (9703100, 0, '叡智の種火');
INSERT INTO items (itemid, importance, description)
VALUES (9703200, 0, '叡智の灯火');
INSERT INTO items (itemid, importance, description)
VALUES (9703300, 0, '叡智の大火');
INSERT INTO items (itemid, importance, description)
VALUES (9703400, 0, '叡智の猛火');
INSERT INTO items (itemid, importance, description)
VALUES (9703500, 0, '叡智の聖火');
INSERT INTO items (itemid, importance, description)
VALUES (9704100, 0, '叡智の種火');
INSERT INTO items (itemid, importance, description)
VALUES (9704200, 0, '叡智の灯火');
INSERT INTO items (itemid, importance, description)
VALUES (9704300, 0, '叡智の大火');
INSERT INTO items (itemid, importance, description)
VALUES (9704400, 0, '叡智の猛火');
INSERT INTO items (itemid, importance, description)
VALUES (9704500, 0, '叡智の聖火');
INSERT INTO items (itemid, importance, description)
VALUES (9705100, 0, '叡智の種火');
INSERT INTO items (itemid, importance, description)
VALUES (9705200, 0, '叡智の灯火');
INSERT INTO items (itemid, importance, description)
VALUES (9705300, 0, '叡智の大火');
INSERT INTO items (itemid, importance, description)
VALUES (9705400, 0, '叡智の猛火');
INSERT INTO items (itemid, importance, description)
VALUES (9705500, 0, '叡智の聖火');
INSERT INTO items (itemid, importance, description)
VALUES (9706100, 0, '叡智の種火');
INSERT INTO items (itemid, importance, description)
VALUES (9706200, 0, '叡智の灯火');
INSERT INTO items (itemid, importance, description)
VALUES (9706300, 0, '叡智の大火');
INSERT INTO items (itemid, importance, description)
VALUES (9706400, 0, '叡智の猛火');
INSERT INTO items (itemid, importance, description)
VALUES (9706500, 0, '叡智の聖火');
INSERT INTO items (itemid, importance, description)
VALUES (9707100, 0, '叡智の種火');
INSERT INTO items (itemid, importance, description)
VALUES (9707200, 0, '叡智の灯火');
INSERT INTO items (itemid, importance, description)
VALUES (9707300, 0, '叡智の大火');
INSERT INTO items (itemid, importance, description)
VALUES (9707400, 0, '叡智の猛火');
INSERT INTO items (itemid, importance, description)
VALUES (9707500, 0, '叡智の聖火');
INSERT INTO items (itemid, importance, description)
VALUES (9750100, 0, '叡智の種火');
INSERT INTO items (itemid, importance, description)
VALUES (9750200, 0, '叡智の灯火');
INSERT INTO items (itemid, importance, description)
VALUES (9750300, 0, '叡智の大火');
INSERT INTO items (itemid, importance, description)
VALUES (9750400, 0, '叡智の猛火');
INSERT INTO items (itemid, importance, description)
VALUES (9750500, 0, '叡智の聖火');
INSERT INTO items (itemid, importance, description)
VALUES (9770100, 0, '叡智の種火');
INSERT INTO items (itemid, importance, description)
VALUES (9770200, 0, '叡智の灯火');
INSERT INTO items (itemid, importance, description)
VALUES (9770300, 0, '叡智の大火');
INSERT INTO items (itemid, importance, description)
VALUES (9770400, 0, '叡智の猛火');
INSERT INTO items (itemid, importance, description)
VALUES (9770500, 0, '叡智の聖火');
INSERT INTO items (itemid, importance, description)
VALUES (9930100, 0, '骸骨兵');
INSERT INTO items (itemid, importance, description)
VALUES (9930200, 0, '骸骨兵');
INSERT INTO items (itemid, importance, description)
VALUES (9930300, 0, '骸骨兵');
INSERT INTO items (itemid, importance, description)
VALUES (9930400, 0, '竜牙兵');
INSERT INTO items (itemid, importance, description)
VALUES (9930500, 0, '竜牙兵');
INSERT INTO items (itemid, importance, description)
VALUES (9930600, 0, '竜牙兵');
INSERT INTO items (itemid, importance, description)
VALUES (9930700, 0, '古代兵');
INSERT INTO items (itemid, importance, description)
VALUES (9930800, 0, '古代兵');
INSERT INTO items (itemid, importance, description)
VALUES (9930900, 0, '古代兵');
INSERT INTO items (itemid, importance, description)
VALUES (9931000, 0, '中世兵');
INSERT INTO items (itemid, importance, description)
VALUES (9931100, 0, '中世兵');
INSERT INTO items (itemid, importance, description)
VALUES (9931200, 0, '中世兵');
INSERT INTO items (itemid, importance, description)
VALUES (9931300, 0, '近代兵');
INSERT INTO items (itemid, importance, description)
VALUES (9931400, 0, '近代兵');
INSERT INTO items (itemid, importance, description)
VALUES (9931500, 0, '近代兵');
INSERT INTO items (itemid, importance, description)
VALUES (9931600, 0, 'ゴースト');
INSERT INTO items (itemid, importance, description)
VALUES (9931800, 0, 'ピクト人');
INSERT INTO items (itemid, importance, description)
VALUES (9931900, 0, 'ピクト人');
INSERT INTO items (itemid, importance, description)
VALUES (9932000, 0, 'ピクト人');
INSERT INTO items (itemid, importance, description)
VALUES (9932100, 0, '悪魔');
INSERT INTO items (itemid, importance, description)
VALUES (9932200, 0, 'オートマタ');
INSERT INTO items (itemid, importance, description)
VALUES (9932300, 0, 'ゴーレム');
INSERT INTO items (itemid, importance, description)
VALUES (9932500, 0, 'ワイバーン');
INSERT INTO items (itemid, importance, description)
VALUES (9932600, 0, 'ドラゴン');
INSERT INTO items (itemid, importance, description)
VALUES (9932610, 0, 'ドラゴン');
INSERT INTO items (itemid, importance, description)
VALUES (9932620, 0, 'ドラゴン');
INSERT INTO items (itemid, importance, description)
VALUES (9932630, 0, 'ドラゴン');
INSERT INTO items (itemid, importance, description)
VALUES (9932640, 0, 'ドラゴン');
INSERT INTO items (itemid, importance, description)
VALUES (9932700, 0, 'アマゾネス');
INSERT INTO items (itemid, importance, description)
VALUES (9932800, 0, 'アマゾネス');
INSERT INTO items (itemid, importance, description)
VALUES (9932900, 0, 'アマゾネス');
INSERT INTO items (itemid, importance, description)
VALUES (9933000, 0, 'ラミア');
INSERT INTO items (itemid, importance, description)
VALUES (9933100, 0, 'ケンタウロス');
INSERT INTO items (itemid, importance, description)
VALUES (9933200, 0, 'ケンタウロス');
INSERT INTO items (itemid, importance, description)
VALUES (9933300, 0, 'ケンタウロス');
INSERT INTO items (itemid, importance, description)
VALUES (9933400, 0, 'キメラ');
INSERT INTO items (itemid, importance, description)
VALUES (9933500, 0, 'スペルブック');
INSERT INTO items (itemid, importance, description)
VALUES (9933600, 0, 'お金ボーナス');
INSERT INTO items (itemid, importance, description)
VALUES (9933700, 0, '経験値ボーナス');
INSERT INTO items (itemid, importance, description)
VALUES (9933710, 0, '経験値ボーナス');
INSERT INTO items (itemid, importance, description)
VALUES (9933720, 0, '経験値ボーナス');
INSERT INTO items (itemid, importance, description)
VALUES (9933730, 0, '経験値ボーナス');
INSERT INTO items (itemid, importance, description)
VALUES (9933740, 0, '経験値ボーナス');
INSERT INTO items (itemid, importance, description)
VALUES (9933750, 0, '経験値ボーナス');
INSERT INTO items (itemid, importance, description)
VALUES (9933760, 0, '経験値ボーナス');
INSERT INTO items (itemid, importance, description)
VALUES (9933770, 0, '経験値ボーナス');
INSERT INTO items (itemid, importance, description)
VALUES (9933800, 0, 'ホムンクルス');
INSERT INTO items (itemid, importance, description)
VALUES (9933900, 0, 'ウェアウルフ');
INSERT INTO items (itemid, importance, description)
VALUES (9934000, 0, 'ウェアウルフ');
INSERT INTO items (itemid, importance, description)
VALUES (9934100, 0, 'ウェアウルフ');
INSERT INTO items (itemid, importance, description)
VALUES (9934200, 0, 'ウェアジャガー');
INSERT INTO items (itemid, importance, description)
VALUES (9934300, 0, 'ウェアジャガー');
INSERT INTO items (itemid, importance, description)
VALUES (9934400, 0, 'ウェアジャガー');
INSERT INTO items (itemid, importance, description)
VALUES (9934500, 0, 'ゴブリン');
INSERT INTO items (itemid, importance, description)
VALUES (9934600, 0, 'ゴブリン');
INSERT INTO items (itemid, importance, description)
VALUES (9934700, 0, 'ゴブリン');
INSERT INTO items (itemid, importance, description)
VALUES (9934800, 0, '魔神柱');
INSERT INTO items (itemid, importance, description)
VALUES (9934810, 0, '魔神柱');
