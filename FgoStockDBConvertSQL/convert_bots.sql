DELETE FROM bots
WHERE userid is NULL OR userid=0;

INSERT INTO accounts (userid, platform, password, masterkey, authkey, secretkey, freestore, sale_state)
SELECT userid, 1, 'saber', masterkey, authkey, secretkey, freestore, 0
FROM bots;

INSERT INTO accounts (userid, platform, password, masterkey, authkey, secretkey, freestore, sale_state)
SELECT userid, 2, 'saber', masterkey, authkey, secretkey, freestore, 0
FROM bots_ip;

-- 导入mstSvt.dat.sql --


CREATE TEMPORARY TABLE account_has_item_tmp (
    account_id INTEGER UNSIGNED NOT NULL,
    item_id INTEGER UNSIGNED NOT NULL
);

INSERT INTO account_has_item_tmp (account_id, item_id)
SELECT accounts.id, items.id
FROM accounts
JOIN bots
ON accounts.userid = bots.userid
JOIN items
ON bots.svt1 = items.itemid;

INSERT INTO account_has_item_tmp (account_id, item_id)
SELECT accounts.id, items.id
FROM accounts
JOIN bots
ON accounts.userid = bots.userid
JOIN items
ON bots.svt2 = items.itemid;

INSERT INTO account_has_item_tmp (account_id, item_id)
SELECT accounts.id, items.id
FROM accounts
JOIN bots_10
ON accounts.userid = bots_10.userid
JOIN items
ON bots_10.s1 = items.itemid;

INSERT INTO account_has_item_tmp (account_id, item_id)
SELECT accounts.id, items.id
FROM accounts
JOIN bots_10
ON accounts.userid = bots_10.userid
JOIN items
ON bots_10.s2 = items.itemid;

INSERT INTO account_has_item_tmp (account_id, item_id)
SELECT accounts.id, items.id
FROM accounts
JOIN bots_10
ON accounts.userid = bots_10.userid
JOIN items
ON bots_10.s3 = items.itemid;

INSERT INTO account_has_item_tmp (account_id, item_id)
SELECT accounts.id, items.id
FROM accounts
JOIN bots_10
ON accounts.userid = bots_10.userid
JOIN items
ON bots_10.s4 = items.itemid;

INSERT INTO account_has_item_tmp (account_id, item_id)
SELECT accounts.id, items.id
FROM accounts
JOIN bots_10
ON accounts.userid = bots_10.userid
JOIN items
ON bots_10.s5 = items.itemid;

INSERT INTO account_has_item_tmp (account_id, item_id)
SELECT accounts.id, items.id
FROM accounts
JOIN bots_10
ON accounts.userid = bots_10.userid
JOIN items
ON bots_10.s6 = items.itemid;

INSERT INTO account_has_item_tmp (account_id, item_id)
SELECT accounts.id, items.id
FROM accounts
JOIN bots_10
ON accounts.userid = bots_10.userid
JOIN items
ON bots_10.s7 = items.itemid;

INSERT INTO account_has_item_tmp (account_id, item_id)
SELECT accounts.id, items.id
FROM accounts
JOIN bots_10
ON accounts.userid = bots_10.userid
JOIN items
ON bots_10.s8 = items.itemid;

INSERT INTO account_has_item_tmp (account_id, item_id)
SELECT accounts.id, items.id
FROM accounts
JOIN bots_10
ON accounts.userid = bots_10.userid
JOIN items
ON bots_10.s9 = items.itemid;

INSERT INTO account_has_item_tmp (account_id, item_id)
SELECT accounts.id, items.id
FROM accounts
JOIN bots_10
ON accounts.userid = bots_10.userid
JOIN items
ON bots_10.s10 = items.itemid;

INSERT INTO account_has_item (account_id, item_id, quantity)
SELECT account_id, item_id, count(*)
FROM account_has_item_tmp
GROUP BY account_id, item_id;


