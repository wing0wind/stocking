import json
import codecs

TEMPLATE_HEAD = """INSERT INTO items (itemid, importance, description)
VALUES (%d, %d, '%s');
"""

with open('mstSvt.dat.json', 'r') as fin, open('mstSvt.dat.sql', 'w') as fout:
    content = fin.read()
    if content[:3] == codecs.BOM_UTF8:
        content = content[3:]
    mstSvt = json.loads(content)

    for i in mstSvt:
        #print '%d - %d - %s' % (i['id'], i['cost'], i['name'])
        #if (i['id'] >= 1000000 and i['cost'] >= 12):
            #print TEMPLATE_HEAD % (i['id'], i['cost'], i['name'])
        #elif (i['id'] < 1000000 and i['cost'] >= 12):
            #print TEMPLATE_HEAD % (i['id'], i['cost'], i['name'])
        sql = TEMPLATE_HEAD % (i['id'], i['cost'], i['name'])
        fout.write(sql.encode('utf8'))
