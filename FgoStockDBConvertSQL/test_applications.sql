-- 查找所有拥有cost值大于12的用户，显示物品数量和名称 --
SELECT accounts.userid, accounts.platform, account_has_item.quantity, items.description
FROM accounts
JOIN account_has_item
ON account_has_item.account_id = accounts.id
JOIN items
ON account_has_item.item_id = items.id
WHERE items.importance > 12;

-- 查找拥有足够数量的多种物品的用户 --
SELECT *
FROM accounts
JOIN account_has_item
ON account_has_item.account_id = accounts.id
JOIN items
ON account_has_item.item_id = items.id
WHERE (items.itemid = 101300 AND account_has_item.quantity >= 1)
   OR (items.itemid = 501400 AND account_has_item.quantity >= 1)
GROUP BY accounts.userid
HAVING COUNT(DISTINCT items.id) = 2;
-- 变种
SELECT *
FROM accounts
JOIN (
    SELECT account_has_item.account_id, items.id
    FROM account_has_item
	JOIN items
	ON account_has_item.item_id = items.id
	WHERE (items.itemid = 101300 AND account_has_item.quantity >= 1)
   		OR (items.itemid = 501400 AND account_has_item.quantity >= 1)
    ) AS item_searched
ON item_searched.account_id = accounts.id
GROUP BY accounts.userid
HAVING COUNT(DISTINCT item_searched.id) = 2;
-- 稀少种，速度直接快了10倍
SELECT *
FROM accounts
JOIN account_has_item AS ahi1 ON ahi1.account_id = accounts.id
JOIN items AS i1 ON ahi1.item_id = i1.id
JOIN account_has_item AS ahi2 ON ahi2.account_id = accounts.id
JOIN items AS i2 ON ahi2.item_id = i2.id
WHERE  (i1.itemid = 101300 AND ahi1.quantity >= 1)
   AND (i2.itemid = 501400 AND ahi2.quantity >= 1)


--搜索一个userid所有卡片
--1
SELECT items.id, items.itemid
FROM accounts
JOIN account_has_item
ON account_has_item.account_id = accounts.id
JOIN items
ON account_has_item.item_id = items.id
WHERE accounts.userid = '7145995' AND accounts.platform = 1;

--2
SELECT items.id, items.itemid
FROM (
    SELECT *
    FROM accounts
    WHERE accounts.userid = '7145995' AND accounts.platform = 1
    ) as u
JOIN account_has_item
ON account_has_item.account_id = u.id
JOIN items
ON account_has_item.item_id = items.id

--3
CREATE TEMPORARY TABLE uid (
	id INTEGER UNSIGNED NOT NULL
);

INSERT INTO uid (id)
SELECT id
FROM accounts
WHERE accounts.userid = '7145995' AND accounts.platform = 1;

SELECT items.id, items.itemid
FROM uid
JOIN account_has_item
ON account_has_item.account_id = uid.id
JOIN items
ON account_has_item.item_id = items.id;

