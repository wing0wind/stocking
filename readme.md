#前置需求
composer

#安装

在项目中的stocking目录下，执行```composer install```安装所有需要的包。
第二步，将.env.example复制一份为.env。
然后，执行```php artisan key:generate```生成app-key。

如果将整个项目放在web服务器的www目录下，此时应该可以访问主页了，根据web服务器的配置，地址可能为
http://localhost/stocking/public

如果按照Wiki中的配置方法进行配置，地址为
http://localhost/stocking/


#数据库配置

于.env文件中设置如下字段

DB_HOST=localhost

//你的HOST

DB_DATABASE=fgo

//库名，请勿改变

DB_USERNAME=root

//你的数据库用户名，我的本地测试环境是直接root无密码

DB_PASSWORD=

//你的数据库密码


接着在你的数据库里建立一个库，名为 fgo
之后进入fgo库，用以下sql 语句建立一个表


```
#!sql

DROP TABLE IF EXISTS `bots`;
CREATE TABLE `bots` (
  `masterkey` varchar(255) NOT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `authkey` varchar(255) DEFAULT NULL,
  `secretKey` varchar(255) DEFAULT NULL,
  `svt1` varchar(255) DEFAULT NULL,
  `svt2` varchar(255) DEFAULT NULL,
  `freestore` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`masterkey`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

```


然后你可以插入一些测试数据到此表中。



之后访问


http://localhost/stocking/public/stockmanger


如果没有出现错误提示并显示了你插入的测试数据，则证明访问数据库成功

至此开发环境构建完成。