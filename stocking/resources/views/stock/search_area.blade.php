<p id="splitChar" hidden>{!!  @$splitChar !!}</p>
<h3>
{!! Form::open(array('method' => 'get')) !!}
 {!! Form::label('uidLabel', 'Uid:') !!}
 {!! Form::text('uid', @$uid) !!}
 {!! Form::submit('Search') !!}
{!! Form::close() !!}
 </h3>
 <h3>
  {!! Form::open(array('method' => 'get')) !!}
   {!! Form::label('itemIdArrayLabel', 'ItemIdArray:') !!}
   {!! Form::textarea('itemIdArray', @$itemIdArray,array('id'=>'itemIdArrayInput')) !!}
   <br />
   {!! Form::select('itemSelector', @$allItemList);!!}
   <button type="button" onclick="addToArray()">Add</button>
   <br />
   {!! Form::submit('Search') !!}
 {!! Form::close() !!}
   </h3>
   <script>
   $("select[name='itemSelector']").change(function(){});
   function addToArray(){
       var appendItemStr=$("select[name='itemSelector']").val();
       var itemArrayText=$("#itemIdArrayInput").val();
       var lastCharOfString=itemArrayText.charAt(itemArrayText.length-1);
       var splitChar=$("#splitChar").text();
       if(itemArrayText.length>0 && lastCharOfString!=splitChar)
       {
          appendItemStr=splitChar+appendItemStr+splitChar;
       }
       else{
          appendItemStr=appendItemStr+splitChar;
       }
       document.getElementById("itemIdArrayInput").value += appendItemStr;
   }
   </script>
