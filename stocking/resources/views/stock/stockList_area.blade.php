<table >
    <tr>
        <th>Uid</th>
        <th>★5Card</th>
        <th>★4Card</th>
        <th>Device</th>
        <th class="act-cell"></th>
        <th class="act-cell"></th>
    </tr>

    @foreach ($accounts as $key=>$account)
    <tr>
        <td>
            <a href="{{ URL('stockmanger/'.$account->userid).'/edit' }}" >{{ $account->userid }}</a>
        </td>
        <td>
            @foreach($itemsArray[$key] as $item)
            <p>{{ $item->description }}</p>
            @endforeach
        </td>
        <td>

        </td>
        <td>
            {!! @$account->platform !!}
        </td>
        <td>
            <a href="{{ URL('stockmanger/'.$account->userid).'/edit' }}" class="btn btn-success">Edit</a>
        </td>
        <td>@if ($account->sale_state == 0)
                <button type="submit" class="btn btn-success">Stock</button>
            @elseif ($account->sale_state == 1)
                <button type="submit" class="btn btn-warning">Selling</button>
            @else
               <button type="submit" class="btn btn-danger">Sold</button>
            @endif
        </td>
    </tr>
    @endforeach

</table>