@extends('stock.base')

@section('rightContent')
    <div class="userDetail">
        <h3>
        {{ @$account->userid }}(Platform:{{ @$account->platform }})
        <br />
        <a href="{{ URL('stockmanger') }}" class="btn btn-success">Return to List</a>
        </h3>
        <div>
        {!! Form::open(array('url'=>'stockmanger/'.@$account->userid,'method' => 'put')); !!}
                    {!! Form::label('descriptionLabel', 'Description:'); !!}
                    {!! Form::textarea('description', @$account->description); !!}
                    {!! Form::label('freestoreLabel', 'freestore:'); !!}
                    {!! Form::text('freestore', @$account->freestore); !!}
                    {!! Form::label('masterkeyLabel', 'MasterKey:'); !!}
                    {!! Form::text('masterkey', @$account->masterkey); !!}
                    {!! Form::label('passwordLabel', 'Password:'); !!}
                    {!! Form::text('password', @$account->password); !!}
                    {!! Form::label('urlLabel', 'URL:'); !!}
                    {!! Form::text('url', @$account->sale_page_url) !!}
                    {!! Form::label('saleLabel', 'Sale State:'); !!}
                    {!! Form::select('sale_state',$saleState, @$account->sale_state); !!}
                    {!! Form::submit('Confirm'); !!}
                {!! Form::close(); !!}
        <hr>
        </div>
        <div class="itemList">
            @foreach ($items as $item)
                <p>({{ $item->importance }}){{ $item->itemid }}</p>
                <p>{{ $item->description }}</p>
                <hr>
            @endforeach
        </div>
    </div>
@endsection