@extends('stock.base')

@section('rightContent')
    @include('stock.search_area')
    @include('stock.stockList_area')
@endsection