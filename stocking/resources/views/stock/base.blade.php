@extends('base.default')

@section('content')
<div id="userhome" class="row">
        <div class="left span3">
            <div class="top">
                <div class="avatar">
                    <img src="" alt="avatar" />
                </div>
                <h1><a class="pushstate-link" href="">none</a></h1>
                <h2>用户组：</h2>
            </div>
            <ul class="menu">
            <li><a class="pushstate-link" href="" data-section="profile">Profile</a></li>;
            </ul>
        </div>
        <div class="right span9">
        @yield('rightContent')
        </div>

    </div>
@endsection
