<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Accounts extends Model
{

    protected $primaryKey = 'id';


    /**
     * 通过一系列道具集合过滤出符合条件的账户。道具集合是一个顺序数组，每个数组元素是一个二元组，key分别是'itemid'和'quantity'，
     * 对应游戏中的卡片ID（不是卡片表的PKid）和数量。过滤得到拥有足够数量各物品的账号。
     * See also: testFWIS
     * @param $itemSet 物品集合
     */
    public static function findWithItemSet($itemSet)
    {
        $numCards = count($itemSet);
        $query = static::select('accounts.*');
        for ($i = 0; $i<$numCards; ++ $i) {
            $query = $query->join("account_has_item as ahi$i", 'accounts.id', '=', "ahi$i.account_id");
            $query = $query->join("items as i$i", "i$i.id", '=', "ahi$i.item_id");
            $query = $query->where("i$i.itemid", "=", $itemSet[$i]['itemid']);
            $query = $query->where("ahi$i.quantity", ">=", $itemSet[$i]['quantity']);
        }
        return $query->get();
    }

    /**
     * 给定userid和platform，返回该账户的所有道具。注意账户以userid和platform唯一确定。
     * @param unknown $userid
     * @param unknown $platform
     */
    public static function findAllCards($userid, $platform)
    {
        $query = static::join('account_has_item', 'accounts.id', '=', 'account_has_item.account_id');
        $query = $query->join('items', 'account_has_item.item_id', '=', 'items.id');
        $query = $query->where('userid', '=', $userid);
        $query = $query->where('platform', '=', $platform);
        return $query->get();
    }

    public static function testFWIS()
    {
        $itemSet = Array(
            Array(
                'itemid' => 101300,
                'quantity' => 1
            ),
            Array(
                'itemid' => 501400,
                'quantity' => 1
            ),
            Array(
                'itemid' => 401400,
                'quantity' => 1
            ),
            Array(
                'itemid' => 601100,
                'quantity' => 1
            )
        );
        return Accounts::findWithItemSet($itemSet);
    }
}
