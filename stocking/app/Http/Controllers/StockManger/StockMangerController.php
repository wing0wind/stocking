<?php

namespace App\Http\Controllers\StockManger;

use App\Accounts;
use Illuminate\Support\Facades\Redirect;
use Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class StockMangerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $userId = Request::input('uid');//get
        $itemIdArrayString=Request::input('itemIdArray');//post
        $splitChar=",";

        $accounts =array();
        $itemsArray=array();
        $allItemList=array('100100'=>'アルトリア・ペンドラゴン',
            '700600'=>'坂田金時',
            '700700'=>'ヴラド三世',
            '900100'=>'ジャンヌ・ダルク',
            '101800'=>'アルテラ',
            '200200'=>'ギルガメッシュ',
            '501900'=>'諸葛孔明〔エルメロイⅡ世〕',

            '100200'=>'アルトリア・ペンドラゴン〔オルタ〕',
            '100300'=>'アルトリア・ペンドラゴン〔リリィ〕',
            '100500'=>'ネロ・クラウディウス',
            '100800'=>'ジークフリート',
            '102600'=>'シュヴァリエ・デオン',
            '200500'=>'アタランテ',
            '200100'=>'エミヤ',
            '300500'=>'エリザベート・バートリー',
            '401700'=>'マリー・アントワネット',
            '401900'=>'マルタ',
            '601000'=>'ステンノ',
            '601700'=>'カーミラ',
            '700100'=>'ヘラクレス',
            '700200'=>'ランスロット',
            '701600'=>'タマモキャット',
            '900300'=>'ジャンヌ・ダルク〔オルタ〕');
        if (is_numeric($userId))
        {
            $accounts = Accounts::where('userid', '=', $userId)->get();
            $data["uid"]=$userId;
            foreach ($accounts as $key=>$account) {
                $itemsArray[$key]=$this->getHighValueCard($account);
            }
        }
        else if(strlen($itemIdArrayString)>0) {
            $itemIdArrayString = trim($itemIdArrayString, ",");
            $itemIdArray = explode($splitChar, $itemIdArrayString);
            $itemArray = array();
            foreach ($itemIdArray as $itemId) {
                $existFlag = false;
                $newMember = array('itemid' => $itemId, 'quantity' => 1);
                foreach ($itemArray as $key => $item) {
                    if ($item["itemid"] == $itemId) {
                        $itemArray[$key]['quantity'] += 1;
                        $existFlag = true;
                        break;
                    }
                }
                if ($existFlag == false) {
                    $itemArray[] = $newMember;
                }
            }
            //print_r($itemArray);
            $accounts = Accounts::findWithItemSet($itemArray);
            foreach ($accounts as $key=>$account) {
                $itemsArray[$key]=$this->getHighValueCard($account);
            }
        }

        $data["splitChar"]=$splitChar;
        $data["allItemList"]=$allItemList;
        $data["itemIdArray"]=$itemIdArrayString;
        $data["itemsArray"]=$itemsArray;
        $data["title"]="Stock Manger";
        $data["welcome"]="Welcome to Stocking";
        $data["accounts"]=$accounts;
        return view('stock.index', $data);
    }

    private function getHighValueCard(Accounts $account){
        $items = Accounts::findAllCards($account->userid, $account->platform);
        $itemsArray=array();
        foreach ($items as $item) {
            if ($item->importance >= 16)
            {
                $itemsArray[]=$item;
            }
        }
        return $itemsArray;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        echo 'create';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        echo 'store';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        return Redirect::to('stockmanger/'.$id.'/edit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //

        $account = Accounts::where('userid', '=', $id)->first();
        if (!$account)
        {
            //TODO 404
        }
        $items = array();
        $data = array();
        if (is_numeric($account->userid))
        {
            $items=Accounts::findAllCards($account->userid,$account->platform);
        }
        $saleState=array('0' => 'In stock', '1' => 'Selling','2'=>'Sold');
        $data['account']=$account;
        $data['items']=$items;
        $data['saleState']=$saleState;
        return view('stock.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //TODO save the data
        echo "update";
        $account = Accounts::where('userid', '=', $id)->first();
        $account->description=Request::input('description');
        $account->freestore=Request::input('freestore');
        $account->masterkey=Request::input('masterkey');
        $account->password=Request::input('password');
        $account->sale_page_url=Request::input('url');
        $account->sale_state=Request::input('sale_state');
        $account->timestamps=false;
        echo $account;
        $account->save();

        return Redirect::to('stockmanger/'.$id.'/edit');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
